rule {
  match {
    kind = "alerting"
  }

  label "severity" {
    severity = "bug"
    required = true
    comment = "The `severity` label must be set to one of `warning`, `critical` or `info`"
    value = "warning|critical|info"
  }

  label "team" {
    severity = "bug"
    # NOTE: we'd like to switch to enforcing but TPA rules currently assume that
    # this label is already present in the timeseries. for now we can at least
    # ensure that the value of the label, if set in the rule, is valid.
    required = false
    comment = "The team label should be one of: TPA, anti-censorship, network-health, network"
    value = "TPA|anti-censorship|network-health|network"
  }

  annotation "summary" {
    severity = "bug"
    required = true
  }

  annotation "description" {
    severity = "bug"
    required = true
  }

  annotation "playbook" {
    severity = "bug"
    required = true
    # NOTE: the TODO value should be removed when every rule initially present
    # has had its rulebook created. see tpo/tpa/prometheus-alerts#16
    #
    # Anti-Censorship team plans to create playbooks:
    #   https://gitlab.torproject.org/tpo/anti-censorship/team/-/issues/140
    value = "TODO|https://gitlab\\.torproject\\.org/.*"
  }
}
