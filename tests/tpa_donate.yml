rule_files:
  - ../rules.d/tpa_donate.rules

evaluation_interval: 1m

tests:
  - interval: 1m
    input_series:
      - series: 'django_http_exceptions_total_by_type_total{alias="donate-01.torproject.org",app="donate-neo",instance="donate.torproject.org:443",job="donate_neo",namespace="prod",team="TPA",type="FileNotFoundError"}'
        values: '0+1x6'
      - series: 'django_http_exceptions_total_by_type_total{alias="donate-01.torproject.org",app="donate-neo",instance="donate.torproject.org:443",job="donate_neo",namespace="prod",team="TPA",type="FileNotFoundError"}'
        values: '0x3 1+1x3'
      - series: 'django_http_exceptions_total_by_type_total{alias="donate-01.torproject.org",app="donate-neo",instance="donate.torproject.org:443",job="donate_neo",namespace="prod",team="TPA",type="Ratelimited"}'
        values: '0+5x6'
    alert_rule_test:
      - eval_time: 6m
        alertname: DjangoExceptions
        exp_alerts:
          - exp_labels:
              severity: warning
              instance: donate.torproject.org:443
              alias: donate-01.torproject.org
              job: donate_neo
              team: TPA
              app: donate-neo
              namespace: prod
              type: FileNotFoundError
            exp_annotations:
              summary: "Site donate-neo generating FileNotFoundError exceptions"
              description: |
                Django site donate-neo (in prod)
                generated 3 exceptions in the last 5
                minutes, of type FileNotFoundError.
              playbook: "https://gitlab.torproject.org/tpo/tpa/team/-/wikis/service/donate#errors-and-exceptions"

  - interval: 1m
    input_series:
      # problems should be notified
      - series: 'donate_transaction_count_total{alias="donate-01.torproject.org",app="donate-neo",instance="donate.torproject.org:443",job="donate_neo",namespace="prod",status="Failed",team="TPA",type="recurring"}'
        values: '0+10x61'
      # error rate not sustained for 1h
      - series: 'donate_transaction_count_total{alias="donate-01.torproject.org",app="donate-nonexistent",instance="donate.torproject.org:443",job="donate_neo",namespace="prod",status="Failed",team="TPA",type="recurring"}'
        values: '0+2x10 21x10 22+2x41'
      # would trigger, but not in namespace prod
      - series: 'donate_transaction_count_total{alias="donate-01.torproject.org",app="donate-neo-staging",instance="donate.torproject.org:443",job="donate_neo",namespace="staging",status="Failed",team="TPA",type="recurring"}'
        values: '0+2x61'
    alert_rule_test:
      - eval_time: 1h1m
        alertname: DonateHighFailureRate
        exp_alerts:
          - exp_labels:
              severity: warning
            exp_annotations:
              summary: "Unusually high failure rate on donate.torproject.org"
              description: |
                More than 1 failed transaction per minute for the last hour
                has been detected on the production donate.torproject.org
                site. Last increase per 10 minutes is 120 and Stripe
                charges us 0.02$ per failed transaction.

                This might be card testing and should be investigated
                promptly.
              playbook: https://gitlab.torproject.org/tpo/tpa/team/-/wikis/service/donate#stripe-card-testing

  - interval: 1m
    input_series:
      - series: 'django_http_requests_latency_seconds_by_view_method_bucket{alias="donate-01.torproject.org",app="donate-neo",instance="donate.torproject.org:443",job="donate_neo",le="+Inf",method="GET",namespace="prod",team="TPA",view="captcha-audio"}'
        values: '0x65'
      - series: 'django_http_requests_latency_seconds_by_view_method_bucket{alias="donate-01.torproject.org",app="donate-neo",instance="donate.torproject.org:443",job="donate_neo",le="0.6",method="GET",namespace="prod",team="TPA",view="captcha-audio"}'
        values: '1+1x65'
    alert_rule_test:
      - eval_time: 1h5m
        alertname: DjangoHighRequestLatency
        exp_alerts:
          - exp_labels:
              severity: warning
              instance: donate.torproject.org:443
              alias: donate-01.torproject.org
              job: donate_neo
              team: TPA
              app: donate-neo
              namespace: prod
            exp_annotations:
              summary: "High latency on site donate-neo (prod)"
              description: |
                99th percentile request latency is greater than 500ms (594ms) over the last 10 minutes on Django
                site donate-neo (prod).
              playbook: https://gitlab.torproject.org/tpo/tpa/team/-/wikis/service/donate#high-latency

  - interval: 1m
    input_series:
      - series: 'civicrm_torcrm_resque_processor_status_up{alias="crm-int-01.torproject.org",instance="crm.torproject.org:443",job="civicrm",namespace="prod",team="TPA"}'
        values: '0x60'
    alert_rule_test:
      - eval_time: 1h
        alertname: CiviCRMKillSwitch
        exp_alerts:
          - exp_labels:
              severity: warning
              instance: crm.torproject.org:443
              alias: crm-int-01.torproject.org
              job: civicrm
              team: TPA
              namespace: prod
            exp_annotations:
              summary: "kill switch triggered on CiviCRM server crm-int-01.torproject.org"
              description: |
                The "kill switch" has been triggered on the CiviCRM production
                server. This means CiviCRM stopped processing donations from
                the donate.torproject.org site, this should be investigated.
              playbook: "https://gitlab.torproject.org/tpo/tpa/team/-/wikis/service/crm#kill-switch-enabled"

  - interval: 1m
    input_series:
      - series: 'civicrm_jobs_status_up{alias="crm-int-01.torproject.org",instance="crm.torproject.org:443",job="civicrm",jobname="fetch_bounces",namespace="prod",team="TPA"}'
        values: '0x60'
      - series: 'civicrm_jobs_status_up{alias="crm-int-01.torproject.org",instance="crm.torproject.org:443",job="civicrm",jobname="rebuild_smart_group_cache",namespace="prod",team="TPA"}'
        values: '0x15 1x45'
    alert_rule_test:
      - eval_time: 1h
        alertname: CiviCRMJobLag
        exp_alerts:
          - exp_labels:
              severity: warning
              instance: crm.torproject.org:443
              alias: crm-int-01.torproject.org
              job: civicrm
              team: TPA
              namespace: prod
              jobname: fetch_bounces
            exp_annotations:
              summary: "CiviCRM job failure"
              description: |
                The CiviCRM job fetch_bounces on crm-int-01.torproject.org
                has been marked as failed for more than 1h. This could be that
                it has not run fast enough, or that it failed.
              playbook: "https://gitlab.torproject.org/tpo/tpa/team/-/wikis/service/crm#job-failures"

  - interval: 1m
    input_series:
      - series: 'probe_success{alias="donate-01.torproject.org",instance="localhost:9115",target="127.0.0.1:6379",job="donate01_blackbox_redis_banner",team="TPA"}'
        values: '0x15'
      - series: 'probe_success{alias="donate-review-01.torproject.org",instance="localhost:9115",target="127.0.0.2:6379",job="donate-review-01_blackbox_redis_banner",team="TPA"}'
        values: '0x5 1x10'
    alert_rule_test:
      - eval_time: 15m
        alertname: RedisBackendUnavailable
        exp_alerts:
          - exp_labels:
              severity: warning
              instance: localhost:9115
              alias: donate-01.torproject.org
              target: 127.0.0.1:6379
              job: donate01_blackbox_redis_banner
              team: TPA
            exp_annotations:
              summary: "Donate frontend donate-01.torproject.org unable to contact redis backend"
              description: |
                The redis backend 127.0.0.1:6379 has been unreachable from
                donate-01.torproject.org for the last 10 minutes from the frontend site.
              playbook: "https://gitlab.torproject.org/tpo/tpa/team/-/wikis/service/donate#redis-is-unreachable-from-the-frontend-server"
