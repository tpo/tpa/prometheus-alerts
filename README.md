# Prometheus targets and alerts rules

This repository contains YAML files which describe a series of targets
(AKA "exporters") and alerting rules. The external Prometheus server
(`prometheus2.torproject.org`) will periodically fetch this repository
([between 4 and 6 hours](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/puppet/#cron-and-scheduling)), and scrape those targets for metrics,
while using the alerting rules for alerts.

Alert recipients are still configured by TPA, file a ticket with TPA
to make changes at that level.

## Alerts

Alerts live in the `rules.d/` directory, which has further documentation.

## Targets

Targets live in the `targets.d/` directory, which has further documentation.

## Unit tests

Alert rules can be tested with unit tests evaluated by the `promtool` command.
All files with names ending in `.yml` in the directory `tests/` get evaluated by
GitLab CI upon sending new commits. This can let you ensure that alerts are
doing what you expect them to.

See [upstream unit testing
documentation](https://prometheus.io/docs/prometheus/latest/configuration/unit_testing_rules/)
for more details about how to write the tests.

### Gotcha: time()

If you are writing a test for an alert that uses the `time()` function in its
`expr`, you might get surprised at first about the values you get from the
function.

`promtool` returns values that look odd at first, but that ensures the values
stay consistent across test runs.

The value returned by `time()` is always a timestamp that corresponds to
`eval_time`. This means that if you set your tests to evaluate at time 0, then
`time()` will get you a value of `0`. Similarly, if you place `eval_time` at
`1h`, the value returned by `time()` will be `3600`.

If you need to place the values of your fixtures (aka `input_series`) to some
time relative to `time()`, one trick is to use negative timestamps. So for
example if you set `eval_time` to 0 and have one fixture with value `-7200`,
then that value can be thought of as "two hours before now" during the test
evaluation.

### Running the tests locally

This repository now has a hook script directory in `.githooks/`. If you checkout
the repository using TPA's `repos` repository, the tests should get run
automatically as a pre-commit hook (provided you have podman installed).

To run the unit tests locally manually, you can use the following method. This
is what the pre-commit hook is doing:

    podman pull quay.io/prometheus/prometheus
    podman run --rm --name prometheus -d -v .:/prometheus-alerts quay.io/prometheus/prometheus
    podman exec -it prometheus promtool test rules /prometheus-alerts/tests/tpa_bacula.yml
    # work some more, then when done stop the prometheus container, which will
    # automatically delete it:
    podman stop prometheus
