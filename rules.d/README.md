# Alerting rules

This directory should contain alerting rules. Each file should end
with `.rules` to be parsed by Prometheus. Syntax for alerting rules is
covered by the [upstream documentation][].

[upstream documentation]: https://prometheus.io/docs/prometheus/latest/configuration/alerting_rules/

How the files here are managed is up to the teams, but it might make sense to
keep one file per team, or have a prefix for teams to make sure there is no
naming clashes. TPA generally follows the naming convention of
`tpa_$exportername.rules`.

Note that the group `name` field is global and should be unique per Prometheus
cluster.

Alerts might not be picked up by Prometheus until the next restart, or when
puppet updates the files on the server.

Alert receivers and routes are defined by TPA in puppet. Check out the
[prometheus service
documentation](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/service/prometheus#alert-routing-details)
for more details.

## Anatomy of a rule

The key part of the alert rule is the `expr` setting which contains a PromQL
expression. When that expression is evaluated to `true` for long enough (the
`for setting`), Prometheus will fire an error and send it to the Alertmanager.

The `for` setting defines how long the `expr` must be true for the alert to
enter the state of `firing`. If the `expr` evaluates to `true`, but the `for`
period has not yet completely elapsed, then the alert is in state `pending` and
is not yet sent to Alertmanager -- we can see the `pending` alerts in
Prometheus. If `expr` stops evaluating to `true` before the `for` period has
completely elapsed, then the alert is removed and is thus never sent to
Alertmanager.

Alerts contain all of the labels that the timeseries extracted from `expr`
contains.

Alerts can define some additional labels that will help Alertmanager routes sort
out what to do with them. In Tor's monitoring setup, two such labels are
particularly important:

* the `severity` label is required and can take the value of either `info`,
  `warning` or `critical`. Alerts of different severity labels can get sent to
  different recipients
* the `team` label is used in alert routes to decide where the message should be
  sent in order to reach the right team
  * if this label is already affixed to the metrics in Prometheus, then the
    alert does not need to define the label. If it's not set in the metrics
    however, the alert should define it

Finally, alerts can have annotations, which are key-value pairs that get
attached to alerts and can then be used when sending them out to recipients
(e.g. in email templates or as webhook parameters).

All labels of an alert can be used in annotations with the go templating
notation using the `$labels` dictionary. For example to add the value of the
`instance` label from the timeseries, the description annotation could contain
`{{ $labels.instance }}` in its value. More values can also be added to
annotations using other dictionaries. See the [upstream documentation][] for
more details.

In Tor's monitoring setup, a small set of annotations are _required_, namely:

* `summary`: A very short description that should catch the receivers'
  attention and should cover the same alert for all possible targets. For
  example: "Service XYZ is not responding"
* `description`: A still somewhat short description of the problem that contains
  more details about the specific issue. This annotation can for example show
  the exact instance, port number or job name for which the alert has fired.
* `playbook`: A URL of the form `https://gitlab.torproject.org/.*` where
  documentation can be read in order to get instructions for how to handle the
  current alert.

## Example rule

Here's a sample rule, the bridgestrap alerting rule:

    groups:
    -name: tpa_node
      rules:
      - alert: RAIDDegraded
        expr: node_md_disks{state="failed"} > 0
        for: 1m
        labels:
          severity: warning
        annotations:
          summary: "RAID array on {{ $labels.alias }} is degraded"
          description: "The {{ $labels.device }} RAID array on {{ $labels.alias }} has failed: {{ $value }} disks failed in device {{ $labels.device }}"
          playbook: "https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/raid#failed-disk"

