# alerts from the node exporter and its text file collector metrics
groups:
- name: tpa_node
  rules:
  - alert: HostDown
    expr: up{job="node"} < 1
    for: 15m
    labels:
      severity: critical
    annotations:
      summary: 'Host {{ $labels.alias }} is not responding'
      description: 'The host {{ $labels.alias }} has stopped responding for more than 15 minutes.'
      playbook: 'https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/incident-response#server-down'

  - alert: JobDown
    expr: up < 1
    for: 15m
    labels:
      severity: warning
    annotations:
      summary: 'Exporter job "{{ $labels.job }}" on {{ $labels.instance }} is down'
      description: 'Exporter job "{{ $labels.job }}" on {{ $labels.instance }} has been unreachable for more than 15 minutes.'
      playbook: "https://gitlab.torproject.org/tpo/tpa/team/-/wikis/service/prometheus#exporter-job-down-warnings"

  - alert: DiskWillFillSoon
    # Note: currently (2025-02-11) these are the mount points used exclusively
    # as bind mounts that are the most popular. We don't want to double- or
    # triple-alert when disks are bind-mounted elsewhere.
    # TODO As of the new exclusions on mountpoint there is a fix that was merged
    # in node_exporter's master branch but was not released yet. When we can
    # upgrade node_exporter to a higher version than 1.8.2 everywhere (if we're
    # lucky in trixie, but possibly later), we should remove the label
    # conditions on node_filesystem_avail_bytes/_size_bytes and rather join it
    # with node_filesystem_mount_info to exclude just the bind mounted devices
    expr: |
      (node_filesystem_readonly != 1)
      and (node_filesystem_avail_bytes > 0)
      and (
        node_filesystem_avail_bytes{mountpoint !~ "/home|/var/lib/postgresql|/var/lib/docker|/var/opt/gitlab/gitlab-rails/shared/artifacts|/var/log/gitlab|/snap/.*"}
        / node_filesystem_size_bytes{mountpoint !~ "/home|/var/lib/postgresql|/var/lib/docker|/var/opt/gitlab/gitlab-rails/shared/artifacts|/var/log/gitlab|/snap/.*"} < 0.2
      )
      and (
        predict_linear(node_filesystem_avail_bytes{mountpoint !~ "/home|/var/lib/postgresql|/var/lib/docker|/var/opt/gitlab/gitlab-rails/shared/artifacts|/var/log/gitlab|/snap/.*"}[24h], 24*60*60)
        <= 0
      )
    for: 1h
    labels:
      severity: warning
    annotations:
      summary: 'Disk {{ $labels.mountpoint }} on {{ $labels.alias }} is almost full'
      description: 'Disk {{ $labels.mountpoint }} on {{ $labels.alias }} will be full in less than 24 hours'
      playbook: "https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/incident-response#disk-is-full-or-nearly-full"
      dashboard: "https://grafana.torproject.org/d/zbCoGRjnz/disk-usage?from=now-7d&to=now&var-instance={{ $labels.alias }}&var-Filters=mountpoint|%3D|{{ $labels.mountpoint | urlquery }}"

  - alert: DiskFull
    # Note: currently (2025-02-11) these are the mount points used exclusively
    # as bind mounts that are the most popular. We don't want to double- or
    # triple-alert when disks are bind-mounted elsewhere.
    # TODO When we can upgrade node_exporter to a version more recent than 1.8.2
    # everywhere, we should remove the label conditions and rather join with
    # node_filesystem_mount_info to exclude just the bind mounted devices
    expr: node_filesystem_avail_bytes{mountpoint !~ "/home|/var/lib/postgresql|/var/lib/docker|/var/opt/gitlab/gitlab-rails/shared/artifacts|/var/log/gitlab|/snap/.*"} == 0
    for: 15m
    labels:
      severity: critical
    annotations:
      summary: 'Disk {{ $labels.mountpoint }} on {{ $labels.alias }} is full'
      description: 'Disk {{ $labels.mountpoint }} on {{ $labels.alias }} has no space available.'
      playbook: "https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/incident-response#disk-is-full-or-nearly-full"
      dashboard: "https://grafana.torproject.org/d/zbCoGRjnz/disk-usage?from=now-7d&to=now&var-instance={{ $labels.alias }}&var-Filters=mountpoint|%3D|{{ $labels.mountpoint | urlquery }}"

  - alert: InodeCountLow
    # vfat shows up in the metrics, but exposes 0 for both metrics which makes
    # no sense.
    expr: |
      (node_filesystem_files_free{fstype!="vfat",mountpoint !~  "/home|/var/lib/postgresql|/var/lib/docker|/var/opt/gitlab/gitlab-rails/shared/artifacts|/var/log/gitlab|/snap/.*g"}
      / node_filesystem_files{fstype!="vfat",mountpoint !~ "/home|/var/lib/postgresql|/var/lib/docker|/var/opt/gitlab/gitlab-rails/shared/artifacts|/var/log/gitlab|/snap/.*"}
      ) < 0.2
    for: 15m
    labels:
      severity: warning
    annotations:
      summary: 'Filesystem {{ $labels.mountpoint }} on host {{ $labels.alias }} has low inode count'
      description: >-
        On host {{ $labels.alias }} the filesystem mounted on {{ $labels.mountpoint }} has
        {{ $value | humanizePercentage }} free inodes left.
      playbook: "TODO"

  - alert: FilesystemReadOnly
    expr: 'node_filesystem_readonly{mountpoint !~ "/snap/.*"} > 0'
    for: 15m
    labels:
      severity: warning
    annotations:
      summary: 'Filesystem {{ $labels.mountpoint }} on host {{ $labels.alias }} is readonly'
      description: >-
        On host {{ $labels.alias }} the filesystem mounted on
        {{ $labels.mountpoint }} is set to readonly, preventing any writes from
        happening.
      playbook: "https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/incident-response#filesystem-set-to-readonly"

  - alert: RAIDDegraded
    expr: node_md_disks{state="failed"} > 0
    labels:
      severity: critical
    annotations:
      summary: "RAID array on {{ $labels.alias }} is degraded"
      description: "The {{ $labels.device }} RAID array on {{ $labels.alias }} has failed: {{ $value }} disks failed in device {{ $labels.device }}"
      playbook: "https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/raid#failed-disk"

  - alert: DRBDDegraded
    expr: count(node_drbd_disk_state_is_up_to_date != 1) by (job, instance, alias, team)
    for: 1h
    labels:
      severity: warning
    annotations:
      summary: "DRBD has {{ $value }} out of date disks on {{ $labels.alias }}"
      description: "Found {{ $value }} disks that are out of date on {{ $labels.alias }}."
      playbook: "https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/drbd#resyncing-disks"

  - alert: PackagesPendingTooLong
    expr: sum by (alias, team) (apt_upgrades_pending) > 0
    # before changing this threshold, look at the history of this
    # query:
    # sum_over_time(ALERTS{alertname="PackagesPendingTooLong"}[1d:1s])/(24*3600)
    # *normally* it shouldn't go over the `for` threshold here
    for: 1d
    labels:
      severity: warning
    annotations:
      summary: "Packages pending on {{ $labels.alias }} for a week"
      description: "There are {{ $value }} pending package upgrades on {{ $labels.alias }} that have not been automatically installed in a week."
      playbook: "https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/upgrades#blocked-upgrades"

  - alert: ObsoletePackages
    expr: apt_packages_obsolete_count > 0
    for: 1d
    labels:
      severity: info
    annotations:
      summary: "Obsolete packages on {{ $labels.alias }}"
      description: "There are {{ $value }} outdated packages on {{ $labels.alias }} that may need to be cleaned up."
      playbook: "https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/upgrades#obsolete-packages"

  - alert: AptUpdateLagging
    # before changing thresholds here, check the metric
    expr: (time() - apt_package_cache_timestamp_seconds) > 2*24*60*60
    # we *do* sometimes go beyond the 24h mark, but barely, we never
    # do so for more than an hour, so give the query some leeway
    #
    # we *could* just do `> 25` above, but it feels less intuitive,
    # and this *is* a somewhat anomalous condition.
    for: 1h
    labels:
      severity: warning
    annotations:
      summary: "Packages lists on {{ $labels.alias }} are out of date"
      description: |
        The package list on {{ $labels.instance }} has not been
        updated in {{ $value | humanizeDuration }}. It might be
        missing critical security updates.
      playbook: "https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/upgrades#out-of-date-package-lists"

  - alert: UnexpectedReboot
    # fabric sets up a 1h-long silence for hosts when we run "expected/planned"
    # reboots. By expiring the alert 10 minutes before that delay, we ensure
    # that hosts rebooted with fabric shouldn't uselessly send an alert.
    expr: node_time_seconds-node_boot_time_seconds <= 60*50
    labels:
      severity: warning
    annotations:
      summary: "Host {{ $labels.alias }} has recently unexpectedly rebooted"
      description: >-
        The host {{ $labels.alias }} was unexpectedly rebooted
        {{ $value | humanizeDuration }} ago.
      playbook: "https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/incident-response#unexpected-reboot"

  # pint ignore/begin
  - alert: NeedsReboot
    expr: |
      # count all hosts per OS version
      count by (team, job, version_codename) (
        # generate only one metric per host that matches the expression
        group by (alias, team, job) (
          (
            # with count, this would generate multiple matches per
            # host, group restricts that to one per host
            (node_reboot_required > 0)
            or (needrestart_kernel_status{needrestart_kernel_status!="current"} > 0)
            or (needrestart_ucode_status{needrestart_ucode_status!="current"} > 0)
          )
        ) * on(alias)
        group_left(version_codename) node_os_info
      )
    labels:
      severity: warning
    annotations:
      summary: "Some {{ $labels.version_codename }} servers need to reboot"
      description: |
        Found pending kernel or microcode upgrades on {{ $value }}
        hosts running {{ $labels.version_codename}}.
      playbook: "https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/reboots"
  # pint ignore/end

  - alert: OutdatedLibraries
    expr: |
      count by (team, job, version_codename) (
        ( needrestart_processes_with_outdated_libraries > 0 )
        * on (instance)
        group_left(version_codename) node_os_info
      )
    for: 1h
    labels:
      severity: warning
    annotations:
      summary: "Servers running {{ $labels.version_codename }} have outdated libraries"
      description: |
        There are {{ $value }} hosts running {{ $labels.version_codename }}
        which are using outdated libraries. Those processes should be
        restarted.
      playbook: "https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/upgrades#special-cases-and-manual-restarts"

  - alert: NodeTextfileCollectorErrors
    expr: node_textfile_scrape_error > 0
    for: 1h
    labels:
      severity: warning
    annotations:
      summary: "Node exporter textfile collector errors on {{ $labels.alias }}"
      description: |
        The textfile collector on {{ $labels.instance }} has {{ $value }}
        errors. This should be investigated as it means other
        monitoring might be failing.
      playbook: "https://gitlab.torproject.org/tpo/tpa/team/-/wikis/service/prometheus#text-file-collector-errors"

  # Note: this is using a different metric from the similarly named alert in
  # tpa_blackox.rules. The metric for local cert expiry is produced by the node
  # exporter with the textfile collector.
  - alert: LocalX509CertNearExpired
    expr: (tpa_cert_expiry_timestamp_seconds - time()) < 28*24*60*60
    labels:
      severity: warning
    annotations:
      summary: "TLS certificate {{ $labels.filename }} on {{ $labels.alias }} is nearing expiry date"
      description: |
        Certificate expiry date of file {{ $labels.filename }} on the host
        {{ $labels.alias }} is less than 28 days. It will expire in
        {{ $value | humanizeDuration }}.
      playbook: "TODO"

  - alert: FilesInVarMail
    expr: (node_directory_inode_count{directory="/var/mail"} - 1) > 0
    labels:
      severity: warning
    annotations:
      summary: "Files are present in /var/mail on {{ $labels.alias }}"
      description: |
        A number of mailboxes ({{ $value }}) have apparead on host {{
        $labels.alias }}. This is a symptom of a misconfigured cron
        job or a missing mail alias, and should be resolved.
      playbook: "https://gitlab.torproject.org/tpo/tpa/team/-/wikis/service/email#files-are-present-in-var-mail"

  - alert: SystemdFailedUnits
    expr: node_systemd_units{state="failed"} > 0
    for: 1h
    labels:
      severity: warning
    annotations:
      summary: "Some systemd units are in failed state on {{ $labels.alias }}"
      description: |
        There are currently {{ $value }} units in a failed state on {{ $labels.alias }}
      playbook: "https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/puppet#failed-systemd-units-on-hosts"

  - alert: HostClockSkew
    expr: ((node_timex_offset_seconds > 0.05 and deriv(node_timex_offset_seconds[5m]) >= 0) or (node_timex_offset_seconds < -0.05 and deriv(node_timex_offset_seconds[5m]) <= 0)) * on(instance) group_left (nodename) node_uname_info{nodename=~".+"}
    for: 15m
    labels:
      severity: warning
    annotations:
      summary: "Host clock skew on {{ $labels.alias }}"
      description: |
        The kernel's clock is skewed by more than 0.05s ({{ $value | humanizeDuration }})
        and continuing to drift. Ensure NTP is configured correctly on {{ $labels.alias }}.
      playbook: "https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/incident-response#host-clock-desynchronized"

  - alert: HostClockNotSynchronizing
    expr: (min_over_time(node_timex_sync_status[1m]) == 0 and node_timex_maxerror_seconds >= 16) * on(instance) group_left (nodename) node_uname_info{nodename=~".+"}
    for: 15m
    labels:
      severity: warning
    annotations:
      summary: "NTP is failing to synchronize the host clock on {{ $labels.alias }}"
      description: |
        Clock not synchronising. Ensure NTP is configured and running properly on {{ $labels.alias }}.
      playbook: "https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/incident-response#host-clock-desynchronized"

  - alert: DiskTemperatureHigh
    expr: avg by (alias, disk, team) (((smartmon_airflow_temperature_cel_raw_value) OR (smartmon_temperature_celsius_raw_value)) > 70)
    for: 15m
    labels:
      severity: warning
    annotations:
      summary: "Disk temperature is running high or device {{ $labels.disk }} for {{ $labels.alias }}"
      description: |
        The temperature of disk {{ $labels.disk }} on {{ $labels.alias }} is higher than 70°C.
        You may want to check with the hoster that there's no ventilation or HVAC issue.
      playbook: "https://gitlab.torproject.org/tpo/tpa/team/-/issues/41682"

  # type="scsi" are usually volumes exposed to ganeti instances by qemu. We only
  # want to alert on disks for physical machines
  - alert: DiskSMARTUnhealthy
    expr: smartmon_device_smart_healthy{type!="scsi"} < 1
    for: 15m
    labels:
      severity: warning
    annotations:
      summary: "SMART reports unhealthy disk for device {{ $labels.disk }} on {{ $labels.alias }}"
      description: |
        SMART has found issues with the disk {{ $labels.disk }} on {{ $labels.alias }}.
        It may indicate an incoming or ongoing disk failure.
      playbook: "TODO"

  - alert: UDLdapLastRefreshTooOld
    expr: (time() - node_path_modification_timestamp_seconds) > 60*60
    labels:
      severity: warning
    annotations:
      summary: "userdir-ldap has not refreshed correctly on {{ $labels.alias }} for more than 1h"
      description: |
        On {{ $labels.alias }} the file {{ $labels.path }} has
        a last modification date that's one hour or more in the past. The userdir-ldap
        replication should be verified.
      playbook: "TODO"
