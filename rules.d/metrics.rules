groups:
- name: metrics
  rules:
  - alert: CollecTor ERRORS
    expr: metrics_log_warnings{alias="CollecTor", level="ERROR"} > 1
    for: 1h
    labels:
      severity: critical
      team: network-health
    annotations:
      title: "CollecTor failures:"
      description: "CollecTor is creating errors importing descriptors."
      summary: "Too many CollecTor failures"
      playbook: "TODO"
      host: "{{$labels.instance}}"
  - alert: Collector Index Stale
    expr: collector_file_freshness{status="critical"} > 0
    for: 10m
    labels:
      severity: critical
      team: network-health
    annotations:
      title: "Collector Index Stale"
      description: "The collector index freshness metric has been in a critical state for more than 10 minutes for {{ $labels.path }} on {{ $labels.hosts }}"
      summary: "Collector index stale for more than 10 minutes"
      playbook: "TODO"
      host: "{{$labels.instance}}"
  - alert: Onionoo ERRORS
    expr: metrics_log_warnings{alias="OnionooService", level="ERROR"} > 1
    for: 1h
    labels:
      severity: critical
      team: network-health
    annotations:
      title: "Onionoo failures"
      description: "Onionoo is creating errors processing descriptors."
      summary: "Too many Onionoo failures"
      playbook: "TODO"
      host: "{{$labels.instance}}"
  - alert: "Onionoo Data Freshness"
    expr: onionoo_data_freshness_seconds > 14400
    for: 1h
    labels:
      severity: critical
      team: network-health
    annotations:
      title: "Onionoo Data Freshness"
      description: "Onionoo data has not been updated for the past 4h"
      summary: "Onionoo data has not been updated for the past 4h or more"
      playbook: "TODO"
      host: "{{$labels.instance}}"
  - alert: Tor DNS EL Freshness
    expr: changes(exits_list_timestamp_seconds[3h]) < 1
    for: 5m
    labels:
      severity: critical
      team: network-health
    annotations:
      title: "Tor DSN EL Freshness"
      description: "Tor DNS Exits List has not been updated for more than 180 minutes"
      summary: "Tor DNS Exits List has not been updated in the last 180 minutes or more"
      playbook: "TODO"
      host: "{{$labels.instance}}"
  - alert: Onionperf rebooted
    expr: node_boot_time_seconds{alias="Onionperf",job="node"} < 600
    for: 5m
    labels:
      severity: critical
      team: network-health
    annotations:
      title: "{{$labels.instance}} rebooted"
      description: "{{$labels.instance}} has been rebooted"
      summary: "{{$labels.instance}} has been rebooted in the last 5m"
      playbook: "TODO"
      host: "{{$labels.instance}}"
  - alert: Onionperf traffic down
    expr: rate(node_network_receive_bytes_total{alias="Onionperf",job="node"}[5m])==0
    for: 5m
    labels:
      severity: critical
      team: network-health
    annotations:
      title: "{{$labels.instance}} traffic down"
      description: "{{$labels.instance}} has not been receiving any traffic"
      summary: "{{$labels.instance}} has not been receiving any traffic in the last 5m"
      playbook: "TODO"
      host: "{{$labels.instance}}"
  - alert: Onionperf disk space
    expr: 100 - ((node_filesystem_avail_bytes{alias="Onionperf",fstype!='tmpfs',fstype!='shm'} * 100) / node_filesystem_size_bytes{alias="Onionperf",fstype!='tmpfs',fstype!='shm'}) > 95
    for: 5m
    labels:
      severity: critical
      team: network-health
    annotations:
      title: "{{$labels.instance}} disk is almost full."
      description: "{{$labels.instance}} disk space used is more than 95%"
      summary: "{{$labels.instance}} is using {{ $value }}% of disk space available."
      playbook: "TODO"
      host: "{{$labels.instance}}"
  - alert: Onionperf client down
    expr: up{alias="Onionperf"} == 0
    for: 5m
    labels:
      severity: critical
      team: network-health
    annotations:
      title: "{{$labels.instance}} is down"
      description: "{{$labels.instance}} client not responding"
      summary: "{{$labels.instance}} is currently down."
      playbook: "TODO"
      host: "{{$labels.instance}}"
  - alert: Relays per country HIGH
    expr: delta(total_network_relays_country{status="all"}[1h:]) > 50
    for: 1h
    labels:
      severity: critical
      team: network-health
    annotations:
      title: Too many relays have joined the network from the same country in the last hour
      summary: More than 50 relays have joined the network from "{{ $labels.country }}"
      playbook: "TODO"
      description: More than 50 relays from "{{ $labels.country }}" have joined the network in the last hour.
  - alert: Relays per country MEDIUM
    expr: delta(total_network_relays_country{status="all"}[24h:]) > 50
    for: 1h
    labels:
      severity: critical
      team: network-health
    annotations:
      title: "Too many relays have joined the network from the same country in the last 24 hours"
      summary: "More than 50 relays have joined the network from {{$labels.country}}"
      playbook: "TODO"
      description: "More than 50 relays from {{$labels.country}} have joined the network in the last 24 hours."
  - alert: Relays per AS HIGH
    expr: delta(total_network_tor_as_relays{status="all"}[1h:]) > 50
    for: 1h
    labels:
      severity: critical
      team: network-health
    annotations:
      title: "Too many relays have joined the network from the same AS in the last hour"
      summary: "More than 50 relays have joined the network from {{$labels.autonomous_system}}"
      playbook: "TODO"
      description: "More than 50 relays from {{$labels.autonomous_system}} have joined the network in the last hour."
  - alert: Relays per AS MEDIUM
    expr: delta(total_network_tor_as_relays{status="all"}[24h:]) > 50
    for: 1h
    labels:
      severity: critical
      team: network-health
    annotations:
      title: "Too many relays have joined the network from the same AS in the last 24 hours"
      summary: "More than 50 relays have joined the network from {{$labels.autonomous_system}}"
      playbook: "TODO"
      description: "More than 50 relays from {{$labels.autonomous_system}} have joined the network in the last 24 hours."
  - alert: Relays per Flag MEDIUM
    expr: delta(total_network_relays_flag{status="all"}[2h:]) > 50
    for: 1h
    labels:
      severity: critical
      team: network-health
    annotations:
      title: "Too many relays have joined the network with the same flag in the last 2 hours"
      summary: "More than 50 relays have joined the network with {{$labels.flag}} flag"
      playbook: "TODO"
      description: "More than 50 relays with {{$labels.flag}} flag have joined the network in the last 2 hours."
  - alert: Relays per nickname prefix MEDIUM
    expr: delta(total_network_tor_nickname_prefix_relays{status="all"}[2h:])>10
    for: 1h
    labels:
      severity: critical
      team: network-health
    annotations:
      title: "Too many relays have joined the network with the same nickname prefix in the last 2 hours"
      summary: "More than 10 relays have joined the network with the nickname prefix {{$labels.nickname_prefix}}"
      playbook: "TODO"
      description: "More than 10 relays have joined the network with the nickname prefix {{$labels.nickname_prefix}} in the last 2 hours."
  - alert: Relays per contact string MEDIUM
    expr: delta(total_network_tor_contact_string_relays{status="all"}[2h:])>10
    for: 1h
    labels:
      severity: critical
      team: network-health
    annotations:
      title: "Too many relays have joined the network with the same contact string."
      summary: "More than 10 relays have joined the network with the contact string {{$labels.contact_string}}"
      playbook: "TODO"
      description: "More than 10 relays have joined the network with the contact string {{$labels.contact_string}} in the last 2 hours."
