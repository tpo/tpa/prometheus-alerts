# alerts from the puppet_reporter report processor. Technically, we're scraping
# the metrics related to the rules in here via the node exporter and its text
# file collector metrics
groups:
  - name: tpa_puppet_reporter
    rules:
      - alert: PuppetCatalogStale
        expr: (time() - puppet_report) > 24*60*60
        labels:
          severity: warning
        annotations:
          summary: "Stale Puppet catalog on {{ $labels.alias }}"
          description: |
            Puppet has not successfully executed and sent its report on {{ $labels.alias }}
            for more than a day ({{ $value | humanizeDuration }}).
          playbook: "https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/puppet#stale-puppet-catalog"

      - alert: PuppetAgentErrors
        expr: puppet_status{state="failed"} > 0 or puppet_transaction_completed < 1
        # NOTE: this delay is not one of the standard values listed on
        # https://gitlab.torproject.org/tpo/tpa/team/-/wikis/service/prometheus#duration
        # We want to trigger after two puppet agent runs to ensure that errors
        # are recurrent. The delay of one day pushes that to a bit less than 3x
        # later which seems a bit too long.
        for: 9h
        labels:
          severity: warning
        annotations:
          summary: "Puppet agent runs have recurring errors on {{ $labels.alias }}"
          description: |
            Puppet agent runs on {{ $labels.alias }} have been reporting failures
            when processing some resources from the catalog for at least one day.
          playbook: "https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/puppet#consult-the-logs-of-past-local-puppet-agent-runs"
