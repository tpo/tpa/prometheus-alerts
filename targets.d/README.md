# Prometheus targets

This directory contains Prometheus targets. Each file should end with
`.yaml` and will be parsed as a [file_sd_config](https://prometheus.io/docs/prometheus/latest/configuration/configuration/#file_sd_config) entry. It should
look something like this:

    # comment
    ---
    - targets:
      - example.com

Then the `targets` list is scraped by Prometheus, in accordance to the
configuration set in the `prometheus.yml` file, currently managed by
TPA inside Puppet (in the `profile::prometheus::server::external`
class).

Note that the protocol (e.g. `HTTPS`) and path suffix
(e.g. `/metrics`) are defined in the `prometheus.yml` file. In the
above example, it might mean that `https://example.com/metrics` gets
scraped. It is not possible to override those fields in the configs
here, that should be done by TPA.

This implies that each target specification (protocol and suffix)
needs a separate configuration in the Prometheus configuration. To add
a new configuration, [file a ticket with TPA](https://gitlab.torproject.org/tpo/tpa/team/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=).

## Current job configurations

At the time of writing, the current jobs are configured in a file that
looks something like the `prometheus-ci.yml` file in the parent
directory. In other words, if you want this, you should name your
target file that:

| Purpose           | Target file pattern       | Actual scrape target             |
|-------------------|---------------------------|----------------------------------|
| Node exporter     | `node_*.yaml`             | `http://example.com/metrics`     |
| HTTPS exporter    | `metrics_standard_*.yaml` | `https://example.com/metrics`    |
| Blackbox exporter | `blackbox_*.yaml`         | `http://localhost:9115/probe`    |
| Snowflake broker  | `snowflake_*.yaml`        | `https://example.com/prometheus` |

Those are just examples, there are more exceptions documented above,
mostly because many services do not have their own hostnames or can
control their exported path (e.g. `/metrics`).

Ideally, all exporters should switch to using the standard `/metrics`
suffix so we wouldn't have to use those hacks. In general, use the
`node_*` or `metrics_standard_*` prefix unless there's a good reason
to diverge.
